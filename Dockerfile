FROM node:12

RUN groupadd -r faucet && useradd --no-log-init -r -g faucet faucet

COPY data /opt/faucet/synthereum-faucet/data
COPY config /opt/faucet/synthereum-faucet/config
COPY src /opt/faucet/synthereum-faucet/src
COPY tsconfig.json /opt/faucet/synthereum-faucet/tsconfig.json
COPY package.json /opt/faucet/synthereum-faucet/package.json

WORKDIR /opt/faucet/synthereum-faucet
RUN yarn install
RUN yarn build

EXPOSE 8080

USER faucet


