import program from 'commander';
import express from 'express';
require('dotenv').config();

import bodyParser from 'body-parser';

import cors from 'cors';

import fs from 'fs';
import BN from 'bn.js';
import { web3 } from './web3';
import { validateEthAddress } from './validator';
import { buildEthWallet, sendEther, getEthBalance } from './ethWallet';
import {
  buildErc20Wallet,
  sendTokens,
  getTokenBalance,
  getTokenName,
} from './erc20Wallet';

program
  .requiredOption('--ethKeystore <string>', 'Path of Eth address keystore')
  .requiredOption(
    '--ethPwd <string>',
    'Path of password of Eth address keystore',
  )
  .requiredOption('--erc20Keystore <string>', 'Path of Erc20 address keystore')
  .requiredOption(
    '--erc20Pwd <string>',
    'Path of password of Erc20 address keystore',
  );

program.parse(process.argv);

interface UserPositions {
  [address: string]: {
    status: string;
    ethTxHash: string;
    erc20TxHash: string;
  };
}

(async () => {
  let usersPosition = JSON.parse(
    fs.readFileSync('./data/userPositions.json').toString(),
  );
  let app = express();
  app.use(bodyParser.json());
  app.use(cors());

  app.post('/getStatus', async function(req, res) {
    try {
      const userAddress = req.body.address;
      if (!userAddress || !validateEthAddress(userAddress)) {
        return res.status(400).json('Invalid Ethereum address');
      }
      const userRecords = usersPosition[userAddress];
      if (userRecords == null) {
        return res.json({ status: 'redeemable' });
      } else {
        return res.json({ status: userRecords.status });
      }
    } catch (error) {
      console.log(error);
      res.status(500).json('Server internal error');
    }
  });

  app.post('/faucetRequest', async function(req, res) {
    try {
      const userAddress = await web3.eth.accounts.recover(
        'Jarvis Synthereum Faucet Request',
        req.body.signature,
      );
      console.log('Redeem request by user ' + userAddress);
      if (userAddress !== req.body.address) {
        return res
          .status(400)
          .json(`Wrong signature for ${req.body.address} address`);
      }
      const userRecords = usersPosition[userAddress];
      if (userRecords != null) {
        return res.status(400).json('You have already redeemed by the faucet');
      } else {
        usersPosition[userAddress] = {
          status: 'redeeming',
          ethTxHash: '',
          erc20TxHash: '',
        };
        fs.writeFileSync(
          './data/userPositions.json',
          JSON.stringify(usersPosition),
        );
        const ethWallet = await buildEthWallet(
          program.ethKeystore,
          program.ethPwd,
        );
        const ethTxHash = await sendEther(
          ethWallet,
          userAddress,
          new BN(process.env.ETH_AMOUNT),
        );
        console.log(
          `Funded user ${userAddress} in tx ${ethTxHash} with ${web3.utils.fromWei(
            process.env.ETH_AMOUNT,
          )} ETH`,
        );
        const actualEthBalance = await getEthBalance(ethWallet);
        console.log(`Actual faucet ETH balance is: ${actualEthBalance}`);
        const erc20Wallet = await buildErc20Wallet(
          program.erc20Keystore,
          program.erc20Pwd,
        );
        const erc20TxHash = await sendTokens(
          erc20Wallet,
          userAddress,
          new BN(process.env.ERC20_AMOUNT),
        );
        const tokenName = await getTokenName();
        console.log(
          `Funded user ${userAddress} in tx ${erc20TxHash} with ${web3.utils.fromWei(
            process.env.ERC20_AMOUNT,
          )} ${tokenName}`,
        );
        const actualErc20Balance = await getTokenBalance(erc20Wallet);
        console.log(
          `Actual faucet ${tokenName} balance is: ${actualErc20Balance}`,
        );
        usersPosition[userAddress] = {
          status: 'redeemed',
          ethTxHash: ethTxHash,
          ethErc20Hash: erc20TxHash,
        };
        fs.writeFileSync(
          './data/userPositions.json',
          JSON.stringify(usersPosition),
        );
      }
    } catch (error) {
      console.log(error);
      delete usersPosition[req.body.address];
      fs.writeFileSync(
        './data/userPositions.json',
        JSON.stringify(usersPosition),
      );
      res.status(500).json('Server internal error');
    }
  });

  const port = process.env.PORT;

  app.listen(port, function() {
    console.log(`Server running at http://localhost:${port}`);
  });
})();
