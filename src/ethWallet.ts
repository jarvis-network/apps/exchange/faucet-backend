import Wallet from 'ethereumjs-wallet';
import fs from 'fs';
import BN from 'bn.js';
import { sign, sendTransaction } from './transactions';
import { web3 } from './web3';

export async function buildEthWallet(
  utcFilePath: string,
  passwordPath: string,
) {
  const utcFile = fs.readFileSync(utcFilePath).toString();
  const password = fs.readFileSync(passwordPath).toString();
  const wallet = Wallet.fromV3(utcFile, password, true);
  return wallet;
}

export async function sendEther(wallet: Wallet, to: string, amount: BN) {
  try {
    const signedTransaction = await sign(
      wallet.getAddress().toString('hex'),
      to,
      new BN(21000),
      '0x',
      amount,
      wallet.getPrivateKey().toString('hex'),
    );
    const receipt = await sendTransaction(signedTransaction.rawTransaction);
    return receipt.transactionHash;
  } catch (error) {
    throw new Error(error);
  }
}

export async function getEthBalance(wallet: Wallet) {
  const balanceInWei = await web3.eth.getBalance(
    wallet.getAddress().toString('hex'),
  );
  return web3.utils.fromWei(balanceInWei);
}

export async function getEthBalanceOfAddress(address: string) {
  const balanceInWei = await web3.eth.getBalance(address);
  return web3.utils.fromWei(balanceInWei);
}
