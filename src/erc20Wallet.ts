import Wallet from 'ethereumjs-wallet';
require('dotenv').config();
import fs from 'fs';
import BN from 'bn.js';
import { sign, sendTransaction } from './transactions';
import { web3 } from './web3';
const erc20Abi = JSON.parse(
  fs.readFileSync('./config/Erc20Abi.json').toString(),
);
const erc20Contract = new web3.eth.Contract(
  erc20Abi,
  process.env.ERC20_CONTRACT_ADDRESS,
);

export async function buildErc20Wallet(
  utcFilePath: string,
  passwordPath: string,
) {
  const utcFile = fs.readFileSync(utcFilePath).toString();
  const password = fs.readFileSync(passwordPath).toString();
  const wallet = Wallet.fromV3(utcFile, password, true);
  return wallet;
}

export async function sendTokens(wallet: Wallet, to: string, amount: BN) {
  try {
    var dataTransfer = web3.eth.abi.encodeFunctionCall(
      {
        name: 'transfer',
        type: 'function',
        inputs: [
          {
            type: 'address',
            name: 'to',
          },
          {
            type: 'uint256',
            name: 'amount',
          },
        ],
      },
      [to, amount.toString()],
    );
    const signedTransaction = await sign(
      wallet.getAddress().toString('hex'),
      erc20Contract.options.address,
      new BN(60000),
      dataTransfer,
      new BN(0),
      wallet.getPrivateKey().toString('hex'),
    );
    const receipt = await sendTransaction(signedTransaction.rawTransaction);
    return receipt.transactionHash;
  } catch (error) {
    throw new Error(error);
  }
}

export async function getTokenBalance(wallet: Wallet) {
  const balanceInWei = await erc20Contract.methods
    .balanceOf('0x' + wallet.getAddress().toString('hex'))
    .call();
  return web3.utils.fromWei(balanceInWei);
}

export async function getEthBalanceOfAddress(address: string) {
  const balanceInWei = await erc20Contract.methods.balanceOf(address).call();
  return web3.utils.fromWei(balanceInWei);
}

export async function getTokenName() {
  const tokenName = await erc20Contract.methods.name().call();
  return tokenName;
}
